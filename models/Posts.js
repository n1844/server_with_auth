const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const postsSchema = new Schema(
    {
      user: { type: String, required: true },
      textarea: { type: String, required: true },
      date: { type: String, required: true },
      time: { type: String, required: true },
    },
    {
      timestamps: true,
    }
  );
  
  const Posts = mongoose.model("posts",postsSchema);
  
  module.exports = Posts;