const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Posts = require("./Posts");


const userSchema = new Schema(
  {
    email: { type: String, required: true },
    username: { type: String, required: true },
    password: { type: String, required: true },
    role: {
      enum: ["admin", "basic", "shelter-owner"],
      type: String,
      default: "basic",
      required: true,
    },
    posts:[{type: mongoose.Types.ObjectId, ref: 'posts'}]
  },
  {
    timestamps: true,
  }
);

const User = mongoose.model("Users", userSchema);

module.exports = User;
