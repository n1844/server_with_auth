const express = require('express');
const postsController = require('../controllers/posts.controller');

const router = express.Router();
router.post('/posts', postsController.postsPost);


module.exports = router;